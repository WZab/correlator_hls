#include <math.h>
#include "correlator_pkg.h"
correlator_out_t correlator(const bx_num_t bx_num, const bx_num_part_t bx_num_part, 
		const tt_track_t tt_track,
		const lut_dta_arr_t lut_dta_arr,
		const muon_stub_arr_t muon_stub_arr
) {
#pragma HLS PIPELINE II=1
#pragma HLS INTERFACE ap_none port = bx_num
#pragma HLS INTERFACE ap_none port = bx_num_part
#pragma HLS INTERFACE ap_none port = tt_track
#pragma HLS INTERFACE ap_none port = lut_dta_arr
#pragma HLS INTERFACE ap_none port = muon_stub_arr
#pragma HLS INTERFACE ap_ctrl_hs port = return
#pragma HLS ARRAY_PARTITION variable=lut_dta_arr complete dim=0
#pragma HLS ARRAY_PARTITION variable=muon_stub_arr complete dim=0
	//Check if the track has at least 4 segments
	correlator_out_t res;
	res = CORRELATOR_RES_EMPTY;
	if(tt_track.nst < 4)
		return res;
	//Get the LUT for the track
	lut_dta_t lut_dta = lut_dta_arr[tt_track.eta][tt_track.pt];
	//Now we browse the layers with muon stubs
	for (int lyr = 0; lyr < COR_MAX_LAYERS; lyr++) {
		//Extrapolate the Phi to the layer
		ttt_phi_t phi_t;
		if(tt_track.ch == CHG_POS)
			phi_t = tt_track.phi + lut_dta.m;
		else
			phi_t = tt_track.phi - lut_dta.m;
		//Now we browse the possible entries in the layer
		//This must be optimized! Linear search is unacceptable in final design!
		ttt_phi_diff_t min_phi_diff;
		ttt_phi_diff_t min_phi_adiff;
		ap_uint<1> not_matched = 1; // Flag informing that no matching was found yet
		for(int ent = 0; ent < LAY_MAX_ENT; ent++) {
			// Read the tested muon stub
			muon_stub_t stub = muon_stub_arr[lyr][ent];
			// Check Eta
			if((tt_track.eta >= stub.eta_from) && (tt_track.eta <= stub.eta_to)) {
				ttt_phi_diff_raw_t phi_diff = tt_track.phi - phi_t;
				ttt_phi_diff_raw_t phi_adiff;
				if(phi_diff >= 0) phi_adiff = phi_diff;
				else phi_adiff = -phi_diff;
				if(not_matched) {
					min_phi_diff = phi_diff;
					min_phi_adiff = phi_adiff;
					//min_ent = ent;
					not_matched = 0;
				} else if (phi_adiff < min_phi_adiff) {
					min_phi_diff = phi_diff;
					min_phi_adiff = phi_adiff;
					//min_ent = ent;
				}
			}
		}
		if(not_matched == 0) {
			// Found the best entry
			int log_p = lut_dta.a * min_phi_diff * min_phi_diff + lut_dta.c;
			if(log_p > 0) {
				//Add the information about the matched stub
				res.sum_log_p += log_p;
				res.n_of_stubs++;
			}
		}
	}
	return res;
};

