/*-------------------------------------------------------------------------------
-- Title      : Correlator package
-- Project    : CMS TRIGGER
-------------------------------------------------------------------------------
-- File       : korelator_pkg.h
-- Author     : WZab
-- Company    : ISE PW, IFD UW
-- Created    : 2019-09-01
-- Last update: 2019-09-06
-- Platform   : 
-- Standard   : HLS C/C++
-------------------------------------------------------------------------------
-- Description: Package defining constants for the golden pattern processor
-------------------------------------------------------------------------------
-- Copyright (c) 2019
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2019-09-01  1.0      WZab    Created
-------------------------------------------------------------------------------
*/
#ifndef __H_CORRELATOR_PKG__
#define __H_CORRELATOR_PKG__
#include <ap_fixed.h>
#include <assert.h>
//Constants and tightly connected types
const int COR_MAX_LAYERS = 8;

const int LAY_MAX_ENT = 18;
typedef ap_uint<5> ent_num_t;

const int N_OF_ETA = 16;
const int N_OF_PT = 64;

//Definition of types
typedef ap_uint<12> bx_num_t;
typedef ap_uint<4> bx_num_part_t;

typedef ap_uint<13> ttt_phi_t;
typedef ap_int<14> ttt_phi_diff_raw_t; //Raw difference between phis
typedef ap_int<9> ttt_phi_diff_t; //Truncated difference betweem phis
typedef ap_int<4> ttt_nst_t; //Number of tracker segments
typedef ap_int<4> ttt_eta_t; //Eta
typedef ap_uint<6> ttt_pt_t; //Pt code
typedef ap_uint<6> ttt_chi2_t;

typedef ap_uint<13> log_p_t; // @!@ That definition may need adjustment!

typedef ap_uint<3> mst_quality_t; //Quality of the muon stub
typedef ap_uint<4> mst_time_in_bx_t; //Time in the BX in 1/16 of BX.
typedef enum {
  CHG_POS = 0,
  CHG_NEG = 1
} ttt_charge_t;
//Definition of the type describing the ttTrack
typedef struct tt_track_t {
  ttt_phi_t phi;
  ttt_eta_t eta;
  ttt_pt_t pt;
  ttt_chi2_t chi2;
  ttt_charge_t ch;
  ttt_nst_t nst;
} tt_track_t;

//Definition of the type describing the muon stubs
typedef struct muon_stub_t {
  ttt_eta_t eta_from;
  ttt_eta_t eta_to;
  ttt_phi_t phi;
  mst_quality_t quality;
  mst_time_in_bx_t time_in_bx;
} muon_stub_t;

//Array type for muon stubs
typedef muon_stub_t muon_stub_arr_t[COR_MAX_LAYERS][LAY_MAX_ENT];

typedef ap_fixed<12,8> lut_dta_a_t;
typedef ap_uint<12> lut_dta_m_t;
typedef ap_uint<12> lut_dta_c_t;

//Definition of the LUT data for the layer
typedef struct {
  lut_dta_a_t a;
  lut_dta_m_t m;
  lut_dta_c_t c; 
} lut_dta_t;
//Definition of the output for the correlator

//Array type for LUT data
typedef lut_dta_t lut_dta_arr_t[N_OF_ETA][N_OF_PT];

typedef struct correlator_out_t {
  int sum_log_p;
  int n_of_stubs;
  ent_num_t entries[COR_MAX_LAYERS];
  log_p_t log_p_vals[COR_MAX_LAYERS];
} correlator_out_t;

const correlator_out_t CORRELATOR_RES_EMPTY = {
		.sum_log_p = 0,
		.n_of_stubs = 0,
};

#endif //__H_CORRELATOR_PKG__
